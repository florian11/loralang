# Lora Lang

A small language I developed as a birthday present for my girlfriend.

See https://florian11.gitlab.io/loralang/embedded.html for a small demo and manual.
